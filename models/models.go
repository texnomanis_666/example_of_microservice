package models

import (
	"time"

	"gorm.io/gorm"
)

type AccessEvents struct {
	gorm.Model

	Id          int64     `json:"id"`
	Number      int64     `json:"number"`
	Type        string    `json:"type"`
	UserGroupID string    `json:"userGroupID"`
	Time        time.Time `json:"timeUTC"`
	Uid         string    `json:"cardUID",gorm:"foreignkey:cardUID"`
	TypeCode    string    `json:"typeCode"`
	DirCode     string    `json:"dirCode"`
}

type AccessGroups struct {
	gorm.Model

	Id             int64  `json:"id"`
	Name           string `json:"name",gorm:"size:100"`
	OrganizationID int64  `json:"organizationId"`
}

type Organizations struct {
	gorm.Model

	Id      int64  `json:"id"`
	Name    string `json:"name",gorm:"size:100"`
	Bin     string `json:"bin",gorm:"unique;size:12"`
	Address string `json:"address"`
	Type    string `json:"type"`
}
type Persons struct {
	gorm.Model

	Id             int64     `json:"id"`
	Name           string    `json:"name",gorm:"size:50"`
	Email          string    `json:"email"`
	PhoneNumber    string    `json:"phoneNumber",gorm:"size:11"`
	BirthDate      time.Time `json:"birthDate",gorm:"type:timestamp"`
	Iin            string    `json:"iin",gorm:"unique;size:12"`
	Status         string    `json:"status",gorm:"size:30"`
	Type           string    `json:"type"`
	LastVersion    int       `json:"version",gorm:"unique"`
	OrganizationID int64     `json:"organization_id"`
}

type PersonCards struct {
	gorm.Model

	Id          int64  `json:"id"`
	CardUID     string `json:"CardUid",gorm:"foreignkey:cardUID", gorm:"type:bytea"`
	Status      string `json:"status",gorm:"size:30"`
	Number      uint   `json:"number"`
	LastVersion int    `json:"version",gorm:"unique"`
	PersonID    int64  `json:"personId",gorm:"foreignkey:PersonID"`
	Name        string `json:"name",gorm:"size:50"`
	UserGroupID string `json:"userGroupID",gorm:"type:text[]"`
}

type ExternalPersonData struct {
	gorm.Model

	Id        int64  `json:"id"`
	PersonID  int64  `json:"personId",gorm:"foreignkey:PersonID"`
	SystemID  string `json:"system_code"`
	ExtID     string `json:"extId"`
	OtherData string `json:"otherData"`
}
type DicSystems struct {
	gorm.Model

	Code string `json:"code",gorm:"unique"`
	Name string `json:"name"`
}

type APITokens struct {
	gorm.Model

	Id             int64  `json:"id"`
	AccessToken    string `json:"accessToken"`
	OrganizationID int64  `json:"organizationId"`
	SystemID       int    `json:"systemId"`
}
