package utils

import (
	"gorm-postgres/models"
	"regexp"
	"strconv"

	"gorm.io/gorm"
)

func CheckIinInDb(Db *gorm.DB, iin string) bool {

	// проверка на наличие такой записи после всех остальных проверок, чтобы не нагружать БД
	err := Db.Model(&models.Persons{}).Where("iin = ?", iin).First(&models.Persons{}).Error
	if err != gorm.ErrRecordNotFound {
		// иин найден - не добавлять
		return true
		//return c.Status(500).JSON("error:dbHaveThisIin")
	} else {
		return false
	}

}

func CheckEmail(str string) bool {
	match, _ := regexp.MatchString(`^[A-Za-z0-9_\-]+@[A-Za-z0-9_\-]+\.[A-Za-z0-9_\-]+$`, str)
	return match
}

func ValidateIIN(iin string) bool {
	// Проверяем контрольную сумму ИИН
	sum := 0
	multipliers := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}
	for i, char := range iin[:10] {
		digit, _ := strconv.Atoi(string(char))
		sum += digit * multipliers[i]
	}
	checksum := sum % 11
	if checksum == 10 {
		// Если контрольная сумма равна 10, то делаем дополнительные вычисления
		sum = 0
		multipliers = []int{3, 4, 5, 6, 7, 8, 9, 10, 11, 1, 2}
		for i, char := range iin[:11] {
			digit, _ := strconv.Atoi(string(char))
			sum += digit * multipliers[i]
		}
		checksum = sum % 11
		if checksum == 10 {
			checksum = 0
		}
	}
	lastDigit, _ := strconv.Atoi(string(iin[11]))
	if checksum == lastDigit {
		return true
	} else {
		return false
	}
}

func CheckPerson(p models.Persons) string {
	const maxLenName int = 50

	text := "OK"

	if CheckEmail(p.Email) == false {
		text = "error:incorrectEmail"
	}

	if len(p.Name) > maxLenName {
		text = "error:veryLongName"
	}

	if ValidateIIN(p.Iin) == false {
		text = "error:iinChecksumError"
	}

	return text
}
