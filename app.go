package main

import (
	"fmt"
	"gorm-postgres/database"
	"gorm-postgres/routes"
	"log"
	"os"
	"sync"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println("Error loading .env file")
	}

	port := os.Getenv("APPLICATION_PORT")
	adminPort := os.Getenv("APPLICATION_ADMIN_PORT")

	database.ConnectDb()
	app := fiber.New()
	adminSync := fiber.New()

	setUpRoutes(app)
	adminSetUpRoutes(adminSync)

	app.Use(handleNotFound)
	adminSync.Use(handleNotFound)

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("port listen")
		if err:=app.Listen(":" + port);err!=nil{
			log.Fatal(err)
		}
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		fmt.Println("Admin Port listen")
		if err:=adminSync.Listen(":" + adminPort);err!=nil{
			log.Fatal(err)
		}


	}()

	wg.Wait()
}

func setUpRoutes(app *fiber.App) {
	app.Post("/persons/create/", routes.CreatePerson)
	app.Put("/persons/:iin/", routes.UpdatePerson)
	app.Post("/skud_callback/", routes.CallbackEvent)
	app.Post("/retrieve_person/", routes.RetrievePerson)
}

func adminSetUpRoutes(app *fiber.App) {
	app.Post("/sync/", routes.CallHTTPMethod)
}

func handleNotFound(c *fiber.Ctx) error {
	return c.SendStatus(404)
}
