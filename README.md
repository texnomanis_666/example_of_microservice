# Education-institution



## Getting started

In order to run a project in a Docker container you need to:

1. Create an .env file in the root folder of the project and copy the contents of example_env file to .env.
2. In the created file declare variables DB_NAME, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT. This is the database connection configuration.
3. Set ADMINER_PORT and APPLICATION_PORT. This is the application connection configuration.
4. Set TOLEM_BASE_URL and TOLEM_AUTH. This is the TOLEM connection configuration.
5. To start the container for the first time in the terminal, enter the command "docker compose up --build" 
6. To start the container "docker compose up -d" 
7. To disable the container "docker compose down --remove-orphans"
8. If there are changes in the project it is necessary to disable the container(5) and start the build again(3)
9. remove all copy of code
10. iin has 12 simbols
11. remove two application and make lister to two ports
12. removed CORS
13. CallHTTPMethod make asychinic
14. remove AddIntervalData function
15. traslate snake_case to camelCase
16. add iin validate


## Dependencies

Here is the complete list of external dependencies used in this project as per the `go.mod` file:

- [Fiber](https://github.com/gofiber/fiber/v2): A fast, Express-like web framework for Go. (v2.48.0)
- [go-postgres](https://github.com/zeimedee/go-postgres): A framework for working with postgres. (v0.0.0-20210622135935-cc5834be70dd)
- [gorm](https://gorm.io/gorm): The fantastic ORM library for Golang. (v1.25.3)
- [gorm.io/driver/postgres](https://pkg.go.dev/gorm.io/driver/postgres): GORM PostgreSQL Driver. (v1.5.2)

Additionally, here are indirect dependencies that are also required by some of the direct dependencies:

- [github.com/andybalholm/brotli](https://pkg.go.dev/github.com/andybalholm/brotli): This package is a brotli compressor and decompressor implemented in Go. (v1.0.5)
- [github.com/google/uuid](https://pkg.go.dev/github.com/google/uuid): The uuid package generates and inspects UUIDs based on RFC 4122 and DCE 1.1: Authentication and Security Services.. (v1.3.0)
- [github.com/jackc/pgpassfile](https://pkg.go.dev/github.com/jackc/pgpassfile): Package pgpassfile is a parser PostgreSQL .pgpass files.. (v1.0.0)
- [github.com/jackc/pgservicefile](https://pkg.go.dev/github.com/jackc/pgservicefile): Package pgservicefile is a parser for PostgreSQL service files. (v0.0.0-20221227161230-091c0ba34f0a)
- [github.com/jackc/pgx/v5](https://pkg.go.dev/github.com/jackc/pgx/v5): A pure Go driver and toolkit for PostgreSQL. (v5.3.1)
- [github.com/jinzhu/inflection](https://pkg.go.dev/github.com/jinzhu/inflection): Inflection pluralizes and singularizes English nouns. (v1.0.0)
- [github.com/jinzhu/now](https://pkg.go.dev/github.com/jinzhu/now): Package for working with DateTime. (v1.1.5)
- [github.com/joho/godotenv](https://pkg.go.dev/github.com/joho/godotenv): A Go port of the Ruby dotenv project (which loads env vars from a .env file).. (v1.5.1)
- [github.com/klauspost/compress](https://pkg.go.dev/github.com/klauspost/compress): This package provides various compression algorithms. (v1.16.5)
- [github.com/mattn/go-colorable](https://pkg.go.dev/github.com/mattn/go-colorable): Colorable writer for windows. (v0.1.13)
- [github.com/mattn/go-isatty](https://pkg.go.dev/github.com/mattn/go-isatty): Package isatty implements interface to isatty. (v0.0.19)
- [github.com/mattn/go-runewidth](https://pkg.go.dev/github.com/mattn/go-runewidth): Provides functions to get fixed width of the character or string. (v0.0.14)
- [github.com/rivo/uniseg](https://pkg.go.dev/github.com/rivo/uniseg): This Go package implements Unicode Text Segmentation according to Unicode Standard Annex #29. (v0.4.4)
- [github.com/robfig/cron/v3](https://pkg.go.dev/github.com/robfig/cron/v3): Cron library for task scheduling. (v3.0.1)
- [github.com/valyala/bytebufferpool](https://pkg.go.dev/github.com/valyala/bytebufferpool): An implementation of a pool of byte buffers with anti-memory-waste protection. (v1.0.0)
- [github.com/valyala/fasthttp](https://pkg.go.dev/github.com/valyala/fasthttp): Fast HTTP implementation for Go. (v1.48.0)
- [github.com/valyala/tcplisten](https://pkg.go.dev/github.com/valyala/tcplisten): Package tcplisten provides customizable TCP net.Listener. (v1.0.0)
- [golang.org/x/crypto](https://pkg.go.dev/golang.org/x/crypto): This repository holds supplementary Go cryptography libraries. (v0.9.0)
- [golang.org/x/sys](https://pkg.go.dev/golang.org/x/sys): This repository holds supplemental Go packages for low-level interactions with the operating system. (v0.10.0)
- [golang.org/x/text](https://pkg.go.dev/golang.org/x/text): This repository holds supplementary Go libraries for text processing, many involving Unicode. (v0.9.0)

Please note that the versions specified here are the ones currently used in this project. It's a good practice to keep your dependencies up to date for security and compatibility reasons. You can use the `go get` command to update dependencies as needed.

## Project Structure


- **data**: Directory for database files and data.
- **database**: Contains database-related code.
  - **database.go**: Database connection and initialization code.
- **models**: Houses data models used in the application.
  - **models.go**: Struct definitions for data models.
- **routes**: Defines API routes and handlers.
  - **routes.go**: Code for defining and handling API routes.
- **schema**: Contains database schema-related files.
  - **20230811161932_init.sql**: SQL script for initializing the database schema.
- **.env**: Configuration file for environment variables.
- **app.go**: Entry point of the application, where the web server is initialized and run.
- **docker-compose.yml**: Docker Compose configuration for setting up a development environment.
- **Dockerfile**: Dockerfile for building a Docker container for the application.
- **go.mod**: Go module file for managing project dependencies.
- **go.sum**: Contains checksums of project dependencies.


## Routes
Postman collection: https://api.postman.com/collections/17276003-4b1b8061-bf6d-491c-bf65-fd37ad931d55?access_key=PMAT-01HAWDQE5J559Z55TE5K9RK2BP

The Authorization value must be passed to the headers of each request, in which you specify the Bearer, then the organization token.

Application defines the following routes in `app.go`:


- **Create Person** (HTTP POST): Create a new person record.
  - Endpoint: `/persons/create/`
  - Method: `POST`
  - Handler Function: `CreatePerson`

- **Update Person** (HTTP PUT): Update an existing person record.
  - Endpoint: `/persons/:id/`
  - Method: `PUT`
  - Handler Function: `UpdatePerson`

- **Callback Event** (HTTP POST): Handle callback events, possibly related to SKUD.
  - Endpoint: `/skud_callback/`
  - Method: `POST`
  - Handler Function: `CallbackEvent`

- **Retrieve Person** (HTTP POST): Retrieve person records based on the last version.
  - Endpoint: `/retrieve_person/`
  - Method: `POST`
  - Handler Function: `RetrievePerson`

- **Call Tolem HTTP Method** (HTTP POST): Update user information from the Tolem system
- Endpoint: `/sync/`
- Method: `POST`
- Handler Function: `CallHTTPMethod`

## Route Descriptions

### Create Person

The Create Person route allows you to create a new person record by sending a POST request with the person's data in the request body.

### Update Person

The Update Person route allows you to update an existing person record by sending a PUT request with the person's ID as a URL parameter and the updated data in the request body.

### Callback Event

The Callback Event route handles callback events, potentially related to SKUD (Security Keycard and Door Access Control). It processes POST requests with event data in the request body.

### Retrieve Person

The Retrieve Person route allows you to retrieve person records based on the last version. Send a POST request with the last version in the request body to get a list of matching person records.


### Call Tolem HTTP Method

Updates user information by sending a request to the Tolem system.