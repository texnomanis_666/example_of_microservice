package routes

import (
	"encoding/json"
	"fmt"
	"gorm-postgres/database"
	"gorm-postgres/models"
	"gorm-postgres/utils"
	"net/http"
	"os"

	//_ github.com/jackc/pgtype

	"github.com/gofiber/fiber/v2"
)

func autorization(c *fiber.Ctx) bool {
	bearerToken := c.Get("Authorization")
	if len(bearerToken) < 7 || bearerToken[:7] != "Bearer " {
		return false
	}
	accessToken := bearerToken[7:]
	var apiToken models.APITokens
	result := database.DB.Db.Where("access_token = ?", accessToken).First(&apiToken)
	if result.Error != nil {
		return false
	}
	return true
}

func CreatePerson(c *fiber.Ctx) error {

	auth := autorization(c)
	if auth != false {
		var person models.Persons
		if err := c.BodyParser(&person); err != nil {
			return c.Status(400).JSON(err.Error())
		}

		text := utils.CheckPerson(person)
		if text != "OK" {
			return c.Status(400).JSON(text)
		}

		if utils.CheckIinInDb(database.DB.Db, person.Iin) == true {
			return c.Status(400).JSON("error:dbHaveThisIin")
		}

		var maxVersion int = 0
		err := database.DB.Db.Model(&models.Persons{}).Select("MAX(last_version)").Scan(&maxVersion).Error
		if err != nil {

		}
		person.LastVersion = maxVersion + 1

		err = database.DB.Db.Create(&person).Error
		if err == nil {
			return c.SendString("{success:True}")
		}
		return c.Status(400).JSON("error:uncorrectData")
	}
	return c.Status(401).JSON("status:401 unautorized")
}

func UpdatePerson(c *fiber.Ctx) error {

	auth := autorization(c)
	if auth != false {
		personID := c.Params("iin")
		var person models.Persons

		if err := c.BodyParser(&person); err != nil {
			return c.Status(400).JSON(err.Error())
		}

		text := utils.CheckPerson(person)
		if text != "OK" {
			return c.Status(400).JSON(text)
		}

		if utils.CheckIinInDb(database.DB.Db, personID) == false {
			return c.Status(400).JSON("error:iinDontFindedInDb")
		}

		var maxVersion int = 0
		err := database.DB.Db.Model(&models.Persons{}).Select("MAX(last_version)").Scan(&maxVersion).Error
		if err != nil {
			return c.Status(500).SendString("Database error")
		}
		person.LastVersion = maxVersion + 1

		if err := database.DB.Db.Save(&person).Error; err != nil {
			return c.Status(500).SendString("Database error")
		}
		return c.Status(200).JSON("status:updated")
	}
	return c.Status(401).JSON("status:401 unautorized")
}

func CallbackEvent(c *fiber.Ctx) error {

	auth := autorization(c)
	if auth != false {
		event := new(models.AccessEvents)
		if err := c.BodyParser(event); err != nil {
			return c.Status(400).JSON(err.Error())
		}
		err := database.DB.Db.Create(&event).Error
		if err == nil {
			return c.SendString("{success:True}")
		} else {
			return c.Status(400).JSON("error:uncorrectData")
		}
	}
	return c.Status(401).JSON("status:401 unautorized")
}

type RetPersons struct {
	Name        string `json:"name"`
	Status      string `json:"status"`
	LastVersion int    `json:"version"`
	Number      int    `json:"number"`
	CardUID     string `json:"cardUid",gorm:"foreignkey:cardUID", gorm:"type:bytea"`
	UserGroupID string `json:"userGroupID",gorm:"type:text[]"`
}

func RetrievePerson(c *fiber.Ctx) error {

	auth := autorization(c)
	if auth != false {
		requestBody := c.Body()
		var jsonBody map[string]interface{}
		if err := json.Unmarshal(requestBody, &jsonBody); err != nil {
			return c.Status(400).JSON(err.Error())
		}

		lastVersion, ok_lastVersion := jsonBody["lastVersion"]
		if !ok_lastVersion {
			return c.Status(400).SendString("Invalid field 'lastVersion'")
		}

		userGroupID, ok_userGroupID := jsonBody["userGroupID"].(string)
		var retPersons []RetPersons

		if !ok_userGroupID && ok_lastVersion {
			err := database.DB.Db.Table("persons").
				Select("persons.name, persons.status, persons.last_version, person_cards.number, person_cards.card_uid, person_cards.user_group_id").
				Joins("JOIN person_cards ON persons.id = person_cards.person_id").
				Where("persons.last_version > ?", lastVersion).
				Scan(&retPersons).Error

			if err == nil {
				return c.JSON(retPersons)
			} else {
				return c.Status(400).SendString("DatabaseError:RetrievePersonWithVersion")
			}
		}

		if ok_userGroupID && ok_lastVersion {

			err := database.DB.Db.Table("persons").
				Select("persons.name, persons.status, persons.last_version, person_cards.number, person_cards.card_uid, person_cards.user_group_id").
				Joins("JOIN person_cards ON persons.id = person_cards.person_id").
				Where("persons.last_version > ?", lastVersion).
				Where("person_cards.user_group_id LIKE ?", "%"+userGroupID+"%").
				Scan(&retPersons).Error
			fmt.Println(err)
			if err == nil {
				return c.JSON(retPersons)
			} else {
				return c.Status(400).SendString("DatabaseError:RetrievePersonWithVersionAndGroup")
			}
		}

	}
	return c.Status(401).JSON("status:401 unautorized")

}

type CardResponse struct {
	UID        string `json:"uid", gorm:"type:bytea"`
	CardNumber int    `json:"cardNumber"`
	StatusCode string `json:"statusCode"`
}

func CallHTTPMethodAsync() bool {
	var persons []models.Persons
	if err := database.DB.Db.Find(&persons).Error; err != nil {
		fmt.Println("Database error")
	}

	for _, person := range persons {
		iin := person.Iin
		if utils.ValidateIIN(iin) == false {
			return false
		}
		baseUrl := os.Getenv("TOLEM_BASE_URL")
		cred := os.Getenv("TOLEM_AUTH")
		req, err := http.NewRequest("GET", fmt.Sprintf(baseUrl, iin), nil)
		if err != nil {
			fmt.Println("Failed to create HTTP request")
			continue
		}
		req.Header.Set("Authorization", "Basic "+cred)
		client := &http.Client{}
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println("Failed to make HTTP request")
			continue
		}
		defer resp.Body.Close()
		var cardResponses []CardResponse
		err = json.NewDecoder(resp.Body).Decode(&cardResponses)
		if err != nil {
			fmt.Println("Failed to decode response JSON")
			continue
		}

		for _, cardResponse := range cardResponses {
			uidBytes := cardResponse.UID
			personCard := models.PersonCards{
				CardUID:     uidBytes,
				Status:      cardResponse.StatusCode,
				Number:      uint(cardResponse.CardNumber),
				LastVersion: person.LastVersion,
				PersonID:    int64(person.ID),
			}
			if err := database.DB.Db.Create(&personCard).Error; err != nil {
				fmt.Println("Failed to save PersonCards")
			}
		}
	}
	return true

}

func CallHTTPMethod(c *fiber.Ctx) error {

	go CallHTTPMethodAsync()

	return c.SendString("{success:True}")

}
